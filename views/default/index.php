<?php

use app\components\TActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

?>
<style>
    .alert-danger {
        position: absolute;
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f5c6cb;
        z-index: 99;
        max-width: 82%;
    }

    div#vs1__combobox {
        border: navajowhite;
        color: #b6dce1 !important;
    }

    span.vs__selected {
        color: #b6dce1;
        font-size: 20px !important;
    }




    .style-chooser .vs__search::placeholder,
    .style-chooser .vs__dropdown-toggle,
    .style-chooser .vs__dropdown-menu {

        border: none;
        color: #394066;
    }

    .style-chooser .vs__clear,
    .style-chooser .vs__open-indicator {
        fill: #394066;
    }

    .vueperslide {
        background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
    }

    .vueperslide__title {
        font-size: 7em;
        opacity: 0.6;
    }

    p {
        position: absolute;
        top: 5px;
        right: 5px;
        z-index: 10;
    }

    html {
        font: 12px Tahoma, Geneva, sans-serif;
    }

    * {
        margin: 0;
        padding: 0;
        color: #fff;
    }

    .vueperslides__bullet .default {
        background-color: rgba(0, 0, 0, 0.3);
        border: none;
        box-shadow: none;
        transition: 0.3s;
        width: 16px;
        height: 16px;
    }

    .vueperslides__bullet--active .default {
        background-color: #42b983;
    }

    .vueperslides__bullet span {
        display: block;
        color: #fff;
        font-size: 10px;
        opacity: 0.8;
    }
</style>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<script src="https://unpkg.com/vue-select@latest"></script>
<link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">

<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/vueperslides"></script>
<link href="https://unpkg.com/vueperslides/dist/vueperslides.css" rel="stylesheet">


<section class="hero-sec hero-sec-change">
    <div id="app" class="container">

        <div v-if="hasError" class="col-12 alert alert-danger">
            <strong>Error!</strong> {{errorMessage}}.
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="batch-r br-changes">
                    <div class="border1 border-change">
                        <div class="inner innaer-4">
                            <div class="border-in-6">
                                <div class="outskew">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="inner-section">
                                                
                                                <div class="payment-form">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-12 col-lg-12">
                                                                <div class="input-form mb-20">
                                                                    <div class="border2 blue-layer new-border">
                                                                        <div class="inner11">
                                                                            <div class="border-in1">
                                                                                <div class="form-group">
                                                                                    <div class="icon-posr">
                                                                                        <input type="text" class="form-control" placeholder="Search Add-Ons" />
                                                                                        <div class="icon-posa">
                                                                                            <img src="<?= $this->theme->getUrl("saloon/images/search_btn_icon_01.png") ?>" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-lg-12">
                                                                <div class="input-form mb-20">
                                                                    <div class="border2 blue-layer new-border">
                                                                        <div class="inner11">
                                                                            <?= $this->render('_product_slider'); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function hasAnyError(dataObject) {
        let errorDetails = {}
        Object.keys(dataObject).map(function(key, index) {
            if (dataObject[key] == null || dataObject[key] === '') {
                errorDetails = {
                    error: true,
                    message: `${key} can not be empty`
                }
            }
        });
        return errorDetails;
    }

    Vue.component('v-select', VueSelect.VueSelect)
    var app = new Vue({
        el: '#app',
        data: {
            hasError: false,
            errorMessage: null,
            selectedStore: '',
            isStoreSelected: false,
            slotId: '',
            name: null,
            gender: '',
            email: null,
            contact: null,
            appointMentDate: null,
            service: '',
            termsAccepted: false,
            servicesList: [{
                    label: 'haricut',
                    id: 'X ca'
                },
                {
                    label: 'hair spa',
                    id: 'D ca'
                },
                {
                    label: 'nail cutttin',
                    id: 'Aca'
                }
            ],
            genderOptions: [{
                    label: 'Male',
                    id: 'male1'
                },
                {
                    label: 'Fe Male',
                    id: 'male2'
                },
                {
                    label: 'Not to Say',
                    id: 'male3'
                }
            ],
            options: [
                'foo',
                'bar',
                'baz'
            ]

        },
        methods: {
            submit: function() {
                const hasError = hasAnyError(this.$data);
                if (hasError.error) {
                    this.hasError = true;
                    this.errorMessage = hasError.message;
                    $('html, body').animate({
                        scrollTop: $("#app").offset().top
                    }, 'slow');
                } else {
                    this.hasError = false;
                    //submit data to server
                }

            },
            loadSlotes: function() {
                console.log("on date change");

            },
            loadServices: async function() {
                const url = "<?= Url::to(['service/list']) ?>"
                const res = fetch(`${url}?store_id=${this.selectedStore}`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                    }).then(response => response.json())
                    .then(result => {
                        this.servicesList = result.list;
                        this.isStoreSelected = result.list.length > 0;
                    })
            },
            getSlots: function() {
                this.isStoreSelected = true;
                console.log("getSlots", this.selectedStore);
            },

        },
        beforeMount() {
            //load data before page load
            console.log("before mount>>>>>>");

        }
    })

    new Vue({
        el: '#app'
    })
</script>