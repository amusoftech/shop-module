<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <?php

    use app\modules\saloon\assets\SaloonAsset;
    use app\assets\AppAsset;
    use app\components\FlashMessage;
    use yii\helpers\Html;

    ?>
    <!-- Metas -->

    <?= Html::csrfMetaTags() ?>

    <!-- Title  -->
    <title><?= Html::encode("Saloon Reservation") ?></title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="#" />

    <link href="<?php echo $this->theme->getUrl('saloon/css/plugins.css') ?>" rel="stylesheet">
    <link href="<?php echo $this->theme->getUrl('saloon/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo $this->theme->getUrl('saloon/css/custom.css') ?>" rel="stylesheet">
    <link href="<?php echo $this->theme->getUrl('saloon/css/responsive.css') ?>" rel="stylesheet">
    <link href="<?php echo $this->theme->getUrl('saloon/css/loader.css') ?>" rel="stylesheet">

    <!--theme color layout-->
    <!-- custom -->

</head>

<body>
    <!-- Start Navbar -->
   
    
        <header id="site-head-main">
            <nav class="navbar navbar-expand-lg d-block">
                <div class="container d-flex justify-content-start align-items-start">
                    <!-- Logo -->
                    <a class="logo" href="/product/list">
                        <div class="imageBox pb-2">
                            <div class="imageInn">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/fb_icon-u1991.png') ?>" alt="Default Image" />
                            </div>
                            <div class="hoverImg">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/fb_icon-u2998-r.png') ?>" alt="Profile Image" />
                            </div>
                        </div>
                        <div class="imageBox">
                            <div class="imageInn">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/ig_icon-u2999.png') ?>" alt="Default Image" />
                            </div>
                            <div class="hoverImg">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/ig_icon-u2002-r.png') ?>" alt="Profile Image" />
                            </div>
                        </div>
                    </a>
                    <button class="navbar-toggler d-block collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"><img src="<?php echo $this->theme->getUrl('saloon/images/toggle.png') ?>" /> </span>
                    </button>
                    <!-- navbar links -->
                    <ul class="navbar-nav justify-content-between flex-row w-md-100 align-items-md-center mobile-not">
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/indoor-playgrounds/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>About </span><br />
                                                <span>Us</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/treetops-licensing/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Licensing</span><br />
                                                <span>Inquires</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/kids-birthday-parties/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Party </span><br />
                                                <span>Bookings</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/playground/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Contact </span><br />
                                                <span>Us</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mobile d-inline-block d-lg-none">
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/indoor-playgrounds/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>About </span><br />
                                                <span>Us</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="#kids">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Licensing</span><br />
                                                <span>Inquires</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/kids-birthday-parties/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Party </span><br />
                                                <span>Bookings</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item nav-b1">
                            <a class="nav-link button-css color-change" href="https://treetopsplayground.com/playground/">
                                <div class="border1">
                                    <div class="inner">
                                        <div class="border-in">
                                            <h2>
                                                <span>Contact </span><br />
                                                <span>Us</span>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="modify collapse" id="navbarSupportedContent" style="">
                    <ul class="navbar-nav modify1">
                        <li class="nav-item nav-b1 main-menu">
                            <a class="nav-link" href="/">
                                Home
                            </a>
                        </li>
                        <li class="nav-item nav-b1 main-menu">
                            <a class="nav-link" href="https://treetopsplayground.com/indoor-playgrounds/">
                                About us
                            </a>
                        </li>
                        <li class="nav-item nav-b1 main-menu">
                            <a class="nav-link" href="https://treetopsplayground.com/treetops-licensing/">
                                Licensing
                            </a>
                        </li>
                        <li class="nav-item nav-b1 main-menu">
                            <a class="nav-link" href="https://treetopsplayground.com/kids-birthday-parties/">
                                Bookings
                            </a>
                        </li>
                        <li class="nav-item nav-b1 main-menu mb-5">
                            <a class="nav-link" href="https://treetopsplayground.com/playground/">
                                Contact Us
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <!-- End Navbar -->
            <div class="middle-img m-auto">
                <img src="<?php echo $this->theme->getUrl('saloon/images/treetopslogo.png') ?>" />

                <h3>Book Now!</h3>
            </div>
        </header>
        <!--    Start Hero  -->
        <section class="hero-sec hero-sec-change">

            <?= FlashMessage::widget(['type' => 'default', /*'position' => 'bottom-right'*/]) ?>
            <?= $content; ?>
        </section>
        <section>
            <div class="footer-top">
                <img src="<?php echo $this->theme->getUrl('saloon/images/shutterstock_201842290.jpg') ?>" />
            </div>
        </section>

        <section>
            <div class="footer">
                <div class="container-fluid">
                    <ul class="d-flex justify-content-between">
                        <li class="">
                            <a class="nav-logo mb-20 mt-20" href="#0">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/fb_icon-u1991.png') ?>" alt="logo" />
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-logo new" href="#0">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/treetops_funogram_footer.png') ?>" alt="logo" />
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-logo mb-20 mt-20" href="#0">
                                <img src="<?php echo $this->theme->getUrl('saloon/images/ig_icon-u2999.png') ?>" alt="logo" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

    <script src="<?php echo $this->theme->getUrl('saloon/js/jquery-3.0.0.min.js') ?>"></script>
    <!-- popper.min -->
    <script src="<?php echo $this->theme->getUrl('saloon/js/bootstrap.min.js') ?>"></script>
   
    <!-- <script src="< ?php echo $this->theme->getUrl('saloon/js/custom.js') ?>"></script> -->
    <script>
        var myVar;
       /*  function myFunction() {
            myVar = setTimeout(showPage, 3000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        } */
    </script>
</body>

</html>