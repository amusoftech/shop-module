<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\modules\shop;
use app\components\TController;
use app\components\TModule;
use app\models\User;
use Yii;

/**
 * Shop module definition class
 */
class Module extends TModule
{
    const NAME = 'Shop';

    public $controllerNamespace = 'app\modules\shop\controllers';
	
	//public $defaultRoute = 'Shop';
	public function init()
    {
        parent::init();  
        if (\Yii::$app instanceof \yii\web\Application) {  
            
            $this->layoutPath = (User::isAdmin() || User::isManager()) ? Yii::$app->view->theme->basePath . '/views/layouts/' :   MODULE_PATH. 'shop/layout';
        }
    }


    public static function subNav()
    {
        return TController::addMenu(\Yii::t('app', 'Shops'), '#', 'key ', Module::isAdmin(), [
           // TController::addMenu(\Yii::t('app', 'Home'), '//Shop', 'lock', Module::isAdmin()),
        ]);
    }
    /*
    public static function dbFile()
    {
        return __DIR__ . '/db/install.sql';
    }
    */
    
   /* public static function getRules()
    {
        return [
            
            'Shop/<id:\d+>/<title>' => 'Shop/post/view',
           // 'Shop/post/<id:\d+>/<file>' => 'Shop/post/image',
           //'Shop/category/<id:\d+>/<title>' => 'Shop/category/type'
        
        ];
    }
    */
    
}
