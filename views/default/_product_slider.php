   <div id="app">
        <vueper-slides 
        class="no-shadow"
        autoplay
  :visible-slides="3"
  slide-multiple
  :gap="3"
  :slide-ratio="1 / 4"
  :dragging-distance="200"
  :breakpoints="{ 800: { visibleSlides: 2, slideMultiple: 2 } }"
       
        >
            <vueper-slide v-for="i in 5" 
            :key="i" 
            :title="i.toString()" 
            
            :content="`Slide title can be HTML.<br>And so does the slide content, <span style='font-size: 1.2em;color: yellow'>why not?</span>`">
            </vueper-slide>
        </vueper-slides>
    </div>


