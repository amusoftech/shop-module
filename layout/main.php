<?php
use app\assets\AppAsset;
use app\components\FlashMessage;
use app\models\User;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

$user = Yii::$app->user->identity;

/* @var $this \yii\web\View */
/* @var $content string */
// $this->title = yii::$app->name;

AppAsset::register ( $this );
?>
<?php

$this->beginPage ()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>

<meta charset="<?=Yii::$app->charset?>" />
	<?=Html::csrfMetaTags ()?>

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content=<?php echo $this->theme->getUrl ( 'img/favicon/' )?>"mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo $this->theme->getUrl ( 'img/favicon/' )?>mstile-310x310.png" />


    <title><?=Html::encode ( $this->title )?></title>
    <?php
				
				$this->head ()?>
    <meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">


<!--common style-->
<!--common style-->
<link
	href="<?php echo $this->theme->getUrl ( 'css/pos.css' )?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl ( 'css/style-admin.css' )?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl ( 'css/style-responsive.css' )?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl ( 'css/font-awesome.css' )?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl ( 'css/layout-theme-blue.css' )?>"
	rel="stylesheet">

<link href="<?php echo $this->theme->getUrl('css/jquery.toast.css')?>" rel="stylesheet">

<?php if(Yii::$app->controller->id === 'pos'){?>
	<link href="<?=$this->theme->getUrl ( 'css/all.css' )?>"
	rel="stylesheet">
	 <link href="<?=$this->theme->getUrl ( 'css/themify-icons.css' )?>"
	rel="stylesheet"> 
<link href="<?=$this->theme->getUrl ( 'css/bootstrap-editable.css' )?>"
	rel="stylesheet"> 
	<!-- <link href="< ?=$this->theme->getUrl ( 'css/loading-full.css' )?>"
	rel="stylesheet">  -->

	

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
<?php }?>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
/* Center the loader */
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #00a65a;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}


#modal-loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #00a65a;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}


@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

#myDiv {
  opacity: 1.0;
  text-align: center;
}
#overlay {
  position: fixed; /* Sit on top of the page content */
  display: none; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0; 
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5); /* Black background with opacity */
  z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}
.my-logo {
    height: 58px;
  //  padding: 2px 5px 2px 1px;
}
</style>
<body  class="sticky-header theme-<?= Yii::$app->view->theme->style ?>">
<div id="loader"></div>
	


<?php

$this->beginBody ();

if (! User::isGuest ()) :
	?>


    <section>
		<!-- sidebar left start-->
		<div class="sidebar-left">
			<!--responsive view logo start-->
			<div class="logo theme-logo-bg visible-xs-* visible-sm-*">
				
				<a href="<?php
	
	echo Url::home ();
	?>"> <span class="brand-name"><?=Html::encode ( \yii::$app->name )?></span>
				</a>
			</div>
			<!--responsive view logo end-->

			<div class="sidebar-left-info">
				<!-- visible small devices start-->
				<div class=" search-field"></div>
				<!-- visible small devices start-->

				<!--sidebar nav start-->
		<?php
	
	if (method_exists ( $this->context, 'renderNav' )) {
		?>
			<?= Menu::widget ( [ 'encodeLabels' => false,'activateParents' => true,'items' => $this->context->renderNav (),'options' => [ 'class' => 'nav  nav-stacked side-navigation' ],'submenuTemplate' => "\n<ul class='child-list'>\n{items}\n</ul>\n" ] );?>
	<?php
	}
	?>
		<!--sidebar nav end-->

			</div>
		</div>
		<!-- sidebar left end-->

		<!-- body content start-->
		<div class="body-content">

			<!-- header section start-->
			<div class="header-section light-color">

				<!--logo and logo icon start-->
				<div class="logo theme-logo-bg hidden-xs hidden-sm">
					<a href="<?php
	
	echo Url::home ();
	?>"> <img class='my-logo' src="<?=$this->theme->getUrl ( 'img/hero.png' )?>?>" alt="<?=\yii::$app->name?>">
					</a>
				</div>


				<!--logo and logo icon end-->

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-outdent"></i></a>
				<!--toggle button end-->

				<!--mega menu start-->

				<!--mega menu end-->
				<div class="notification-wrap">



					<!--right notification start-->
					<div class="right-notification">
						<ul class="notification-menu">
							<li>
    						<?php if (YII_ENV == 'dev') : ?>
    								<a><div class="label label-danger" role="alert">
									<strong> Warning! </strong> Development Mode is On.
								</div></a>
    						<?php endif; ?>
							</li>
							<li><a><span class="label label-primary" role="version"> Version <?= VERSION ?></span></a></li>
							<li><a><span id="SHIFT_CASH_COUNT" class="label label-danger fa fa-print" role="version"> SHIFT CASH COUNT </span></a></li>

							<!-- <div class="navbar-text">
							  /* echo \lajax\languagepicker\widgets\LanguagePicker::widget ( [
							 		'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
							 		'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE

							 ]);
							 </div> -->
							<li><a href="javascript:;"
								class="btn btn-default dropdown-toggle" data-toggle="dropdown">

								<?= \Yii::$app->user->identity->displayImage(\Yii::$app->user->identity->profile_file, ['width' => '50', 'height' => '50', 'class' => '']); ?>
								
                                <?php echo Yii::$app->user->identity->full_name; ?>
                                <span class=" fa fa-angle-down"></span>
							</a>
								<ul class="dropdown-menu dropdown-usermenu purple pull-right">
									<li><a
										href="<?php
	
	echo Url::toRoute ( [ 
			'/user/view',
			'id' => Yii::$app->user->id 
	] );
	?>"> <i class="fa fa-user pull-right"></i> Profile
									</a></li>
									<li><a
										href="<?php
	
	echo Url::toRoute ( [ 
			'/user/changepassword',
			'id' => Yii::$app->user->id 
	] );
	?>"> <span class="fa fa-key pull-right"></span> <span>Change Password</span>
									</a></li>
									<li><a
										href="<?php
	
	echo Url::toRoute ( [ 
			'/user/update',
			'id' => Yii::$app->user->id 
	] );
	?>"> <span class="fa fa-pencil pull-right"></span> Update
									</a></li>
									<li><a
										href="<?php
	
	echo Url::toRoute ( [ 
			'/user/logout' 
	] );
	?>"> <i class="fa fa-sign-out pull-right"></i> Log Out
									</a></li>
								</ul></li>


						</ul>

					</div>
					<!--right notification end-->
				</div>


			</div>

			<!-- header section end-->

			<!-- page head start-->
			 <?=Breadcrumbs::widget ( [ 'links' => isset ( $this->params ['breadcrumbs'] ) ? $this->params ['breadcrumbs'] : [ ] ] )?>
			<!--body wrapper start-->
			<section class="main_wrapper">
				
				<?php
	if (yii::$app->hasModule ( 'shadow' )) {
		echo app\modules\shadow\components\ShadowWidget::widget ();
	}
	?>
				
			
			

			   <?= FlashMessage::widget (['type' => 'default', /*'position' => 'bottom-right'*/  ]) ?>
			   			   
               <?=$content; ?>



			</section>
			<div class="container hide">
			<div class="dash uno"></div>
			<div class="dash dos"></div>
			<div class="dash tres"></div>
			<div class="dash cuatro"></div>
			</div>
			<footer>

				<div class="text-center">
					<p target="_blank">
					<?php
	
	echo ' &copy; ' . date ( 'Y' ) . ' ' . Yii::$app->params['copy_right'] . ' | All Rights Reserved ';
	?></p>

				</div>


			</footer>
			<!--footer section start-->
			<!--footer section end-->
			<!--body wrapper end-->
		</div>

<input type="hidden" value="<?=Url::base(true)?>"  id="BASEURL">
		<!-- body content end-->

	</section>

<?php
if(Yii::$app->controller->id != 'pos'){?>
<canvas id="signature-pad" class="signature-pad" width=600 height=200></canvas>
<?php }
?>
	<!--common scripts for all pages-->
	<script src="<?php
	
	echo $this->theme->getUrl ( 'js/scripts.js' )?>"></script>

	<script src="<?php
	
	echo $this->theme->getUrl ( 'js/custom-modal.js' )?>"></script>
<!-- <script src="< ?= $this->theme->getUrl ( 'js/all.js' )?>"></script> -->
<script src="<?= $this->theme->getUrl ( 'js/iframeResizer.contentWindow.min.js' )?>"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="<?= $this->theme->getUrl ( 'js/pos.js' )?>"></script>
 <script src="<?= $this->theme->getUrl ( 'js/membership.js' )?>"></script>

 
 <script src="<?= $this->theme->getUrl ( 'js/bootstrap-editable.min.js' )?>"></script>
 <script src="<?= $this->theme->getUrl ( 'js/jquery.toast.js' )?>"></script>

 <!-- 
 <script type="text/javascript" src="< ?= $this->theme->getUrl ( 'js/barcode/jquery-barcode.min' )?>"></script> -->
<script type="text/javascript" src="<?= $this->theme->getUrl ( 'js/barcode/jquery-barcode.js' )?>"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="<?= $this->theme->getUrl ('js/jquery-printme.js')?>"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(".child-list").find('span').contents().unwrap();
});

</script>



<?php
endif;
$this->endBody ();
?>


<script>
	$( document ).ready(function() {
    $(".overlay").show();


});
/*window.onbeforeunload = function (e) {
    return "Please click 'Stay on this Page' and we will give you candy";
};*/

</script>
</body>



<?php

$this->endPage ()?>

</html>

